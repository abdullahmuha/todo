import React from 'react';
import '../App.css';

const person = (props) => {
    return (
        <div className="LineStyle">
            <p onClick={props.click}>i'm {props.name} and i am {props.age}</p>
            <p>{props.children}</p>
            <input type="text" onChange={props.changed} value={props.name}/>
        </div>
    )

};
export default person;