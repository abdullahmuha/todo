import React, { Component } from 'react';
import './App.css';
import Person from './components/Person.js'
class App extends Component {
  state = {
    persons: [
      {
        id: '1', name: 'abdu', age: 20
      },
      {
        id: '2', name: 'Muha', age: 22
      },
      {
        id: '3', name: 'Sjere', age: 28
      }
    ],
    showPerson: true
  }
  deletePersonHandler = (indexPerson) => {
  
    const persons = [...this.state.persons];
    const p = persons.filter( (el) =>{
      return el.id !== indexPerson
    })
    this.setState({ persons: p });
  }
  togglePersonsHandler = () => {
    this.setState({
      showPerson: !this.state.showPerson

    })
  }
  nameChangeHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });
    const person = { ...this.state.persons[personIndex] };
    person.name = event.target.value;
    const persons = [...this.state.persons];
    persons[personIndex] = person;
    this.setState({ persons: persons });
  }
  render() {
    let persons = null;
    if (this.state.showPerson) {
      persons = (
        <div>
          {this.state.persons.map((single) => {
            return <Person
              click={() =>this.deletePersonHandler(single.id)}
              name={single.name}
              age={single.age}
              key={single.id}
              changed={(event) => this.nameChangeHandler(event, single.id)}
            />
          })}
        </div>

      );
    }
    return (
      <div className="App">
        <h1>My first React</h1>
        <button onClick={this.togglePersonsHandler}>Toggle component</button>
        {persons}


      </div>
    )
  }
}

export default App;

// const App = props => {
//   const [old_state, setPersonState] = useState({
//     persons: [
//       {
//         name: 'abdu', age: 20
//       },
//       {
//         name: 'Muha', age: 22
//       },
//       {
//         name: 'Sjere', age: 28
//       }
//     ]
//   });
//   const switchNameHandler = () => {
//     setPersonState({
//       persons: [
//         { name: 'swarts', age: 11 },
//         { name: 'swarts', age: 11 },
//         { name: 'swarts', age: 11 }
//       ]
//     }
//     )
//   }
//   return (
//     <div className="App">
//       <h1>My first React</h1>
//       {<button onClick={switchNameHandler}>Switch name</button>}
//       <Person name={old_state.persons[0].name} age={old_state.persons[0].age}  > and i like programming</Person>
//       <Person name={old_state.persons[1].name} age={old_state.persons[1].age} />
//       <Person name={old_state.persons[2].name} age={old_state.persons[2].age} />
//     </div>
//   )
// }
// export default App;